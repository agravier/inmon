#include "inmon.hpp"
#include <iostream>
#include <fstream>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/ref.hpp>
#include <boost/thread.hpp>
#include <alproxies/almemoryproxy.h>
#include <alcommon/albroker.h>
#include <qi/log.hpp>

static const char intertialSensorXKey[] =
    "Device/SubDeviceList/InertialSensor/AngleX/Sensor/Value";
static const char intertialSensorYKey[] =
    "Device/SubDeviceList/InertialSensor/AngleY/Sensor/Value";

struct InertialMonitor::Impl {
  boost::shared_ptr<AL::ALMemoryProxy> memoryProxy;
  InertialMonitor &module;

  boost::posix_time::time_duration samplingPeriod;
  boost::thread *t;  // thread of the sampling code
  bool stopThread;   // flag to stop the thread, and predicate against spurious unblocks
  boost::mutex stopThreadLock; // lock
  boost::condition_variable condVar; // mechanism to control the thread's execution

  bool outputFile_p;
  std::ofstream outputFile;
  boost::mutex outputFileLock;

  Impl(InertialMonitor &mod)
    : module(mod), outputFile_p(false), outputFile(), t(NULL),
      samplingPeriod(boost::posix_time::milliseconds(100)) {
    try {
      memoryProxy = boost::shared_ptr<AL::ALMemoryProxy>(
            new AL::ALMemoryProxy(mod.getParentBroker()));
    } catch (const AL::ALError& e) {
      qiLogError("InertialMonitor") << "Error while getting proxy of ALMemory: "
                                    << e.toString() << std::endl;
      throw InertialMonitorError("Could not get a proxy to the memory module");
    }
    if (!memoryProxy) {
      qiLogError("InertialMonitor") << "Failed to get a proxy of ALMemory" << std::endl;
      throw InertialMonitorError("Could not get a proxy to the memory module");
    }
  }

  ~Impl() {
    closeOutputFile();
    stopThreadLock.lock();
    stopThread = true;
    stopThreadLock.unlock();
    if (t) {
      t->join();
    }
  }

  void setupOutputFile(const std::string filename) {
    try {
      closeOutputFile();
      outputFileLock.lock();
      outputFile.open(filename.c_str(), std::ios::app); // append
      outputFile_p = true;
      outputFileLock.unlock();
    } catch (std::exception& e) {
      qiLogError("InertialMonitor") << "Failed to open the output file: "
                                    << e.what() << std::endl;
      throw new InertialMonitorError("Failed to open the output file");
    }
  }

  void closeOutputFile() {
    try {
      outputFileLock.lock();
      if (outputFile_p) {
        outputFile.close();
        outputFile_p = false;
      }
      outputFileLock.unlock();
    } catch (std::exception& e) {
      qiLogError("InertialMonitor") << "Failed to close the output file: "
                                    << e.what() << std::endl;
      throw new InertialMonitorError("Failed to close the output file");
    }
  }

  void operator()() {
    boost::unique_lock<boost::mutex> fileLock(outputFileLock); // starts locked
    bool stopThreadCopy;
    boost::posix_time::ptime time_t_epoch(boost::gregorian::date(1970,1,1));
    float x, y;
    stopThreadLock.lock();
    stopThreadCopy = stopThread;
    stopThreadLock.unlock();
    float *intertialSensorX =
        static_cast<float*>(memoryProxy->getDataPtr(intertialSensorXKey));
    float *intertialSensorY =
        static_cast<float*>(memoryProxy->getDataPtr(intertialSensorYKey));
    boost::system_time tickTime = boost::get_system_time();
    while (!stopThreadCopy) {
      // get values
      x = *intertialSensorX;
      y = *intertialSensorY;
      // note the time (in nanoseconds since Jan 1 1970)
      boost::posix_time::time_duration duration = boost::get_system_time() - time_t_epoch;
      long long timestampNanos = duration.total_nanoseconds();
      // record them
      if (outputFile_p) {
        try {
          outputFile << timestampNanos << ", " << x << ", " << y << "\n"; // std::endl flushes
        } catch (std::exception& e) {
          qiLogError("InertialMonitor") << "Failed to write \"" <<  timestampNanos << ", "
                                        << x << ", " << y << "\" to the output file: "
                                        << e.what() << std::endl;
        }
      }
      // calculate next tick time
      tickTime += samplingPeriod;
      // wait for timeout, unlock fileLock while waiting, and control for spurious wakes
      // by checking stopThread.
      condVar.timed_wait(fileLock, tickTime, boost::lambda::var(stopThread));
      stopThreadLock.lock();
      stopThreadCopy = stopThread;
      stopThreadLock.unlock();
    }
  }
};




InertialMonitor::InertialMonitor(boost::shared_ptr<AL::ALBroker> pBroker,
                                 const std::string& pName)
  : AL::ALModule(pBroker, pName) {
  // This description will appear on the naoqi webpage.
  setModuleDescription("Monitor and record intertial sensors.");

  functionName("startMonitor", getName(), "Start or restart the monitor with the given "
               "sampling period, without recording the data locally.");
  addParam("milli", "sampling period in milliseconds");
  BIND_METHOD(InertialMonitor::startMonitor);

  functionName("startMonitorAndRecord", getName(), "Start or restart the monitor with "
               "the given sampling period, recording the data locally in the given plain "
               "text output file. The data are appended to already existing files.");
  addParam("milli", "sampling period in milliseconds");
  addParam("file", "name of the local file where recorded data is to be appended");
  BIND_METHOD(InertialMonitor::startMonitorAndRecord);

  functionName("stopMonitor", getName(), "Stop the monitor and close the record file if "
               "any.");
  BIND_METHOD(InertialMonitor::stopMonitor);
}


InertialMonitor::~InertialMonitor() {}


/**
 * Overrides ALModule::init(). This is called right after the module has been constructed.
 */
void InertialMonitor::init() {
  try {
    impl = boost::shared_ptr<Impl>(new Impl(*this));
    AL::ALModule::init();
  } catch (std::exception& e) {
    qiLogError("InertialMonitor") << "Failed to initialize InertialMonitor module: "
                                  << e.what() << std::endl;
    exit();
  }
}

/** Overrides ALModule::exit(), called before unloading */
void InertialMonitor::exit() {
  AL::ALModule::exit();
}


/**
 * Start or restart the monitor with the given sampling period, without recording the
 * data locally.
 */
void InertialMonitor::startMonitor(const int &milli) {
  stopMonitor();
  qiLogVerbose("InertialMonitor") << "startMonitor with a period of " << milli << " ms";
  try {
    impl->samplingPeriod =  boost::posix_time::milliseconds(milli);
    impl->stopThreadLock.lock();
    impl->stopThread = false;
    impl->stopThreadLock.unlock();
    impl->t = new boost::thread(boost::ref(*impl));
  } catch (std::exception& e) {
    qiLogError("InertialMonitor") << "Failed to start intertial monitor: "
                                  << e.what() << std::endl;
    exit();
  }
}


/**
 * Start or restart the monitor with the given sampling period, recording the data
 * locally in the given plain text output file. The data are appended to the file if it
 * already exists.
 */
void InertialMonitor::startMonitorAndRecord(const int &milli, const std::string &file) {
  stopMonitor();
  qiLogVerbose("InertialMonitor") << "startMonitorAndRecord with a period of " << milli
                                  << " ms and " << file << " as output file ";
  try {    
    impl->samplingPeriod =  boost::posix_time::milliseconds(milli);
    impl->setupOutputFile(file);
    impl->stopThreadLock.lock();
    impl->stopThread = false;
    impl->stopThreadLock.unlock();
    impl->t = new boost::thread(boost::ref(*impl));
  } catch (std::exception& e) {
    qiLogError("InertialMonitor") << "Failed to start intertial monitor with logging: "
                        << e.what() << std::endl;
    exit();
  }
}


/**
 * Stop the monitor, close the record file if any.
 */
void InertialMonitor::stopMonitor() {
  qiLogVerbose("InertialMonitor") << "stopMonitor";
  try {
    impl->closeOutputFile();
    impl->stopThreadLock.lock();
    impl->stopThread = true;
    impl->stopThreadLock.unlock();
    // wake up, it time ...
    impl->condVar.notify_one();
    // ... to die
    if (impl->t) {
      impl->t->join();
    }
  } catch (std::exception& e) {
    qiLogError("InertialMonitor") << "Failed to stop intertial monitor: "
                        << e.what() << std::endl;
    exit();
  }
}


InertialMonitorError::InertialMonitorError(const char* message)
  : msg(message) {}

InertialMonitorError::InertialMonitorError(const std::string message)
  : msg(message.c_str()) {}

const char* InertialMonitorError::what() { return msg; }


