#include <iostream>
#include <boost/program_options.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/algorithm/string.hpp> // toupper
#include <alcommon/albroker.h>
#include <alcommon/albrokermanager.h>
#include <alerror/alerror.h>
#include <alcommon/alproxy.h>

typedef enum {
  START,
  STOP
} command_t;

void parseOpt(std::string *naoBrokerIP, int *naoBrokerPort, command_t *command,
              int *milli, std::string *ouputFilename, int argc, char* argv[]) {
  std::string command_string;
  namespace po = boost::program_options;
  po::options_description desc("Allowed options");
  desc.add_options()
      ("pip", po::value<std::string>(naoBrokerIP)->default_value("nao.local"),
       "IP of the parent broker. Default: nao.local")
      ("pport", po::value<int>(naoBrokerPort)->default_value(9559),
       "Port of the parent broker. Default: 9559")
      ("command,c", po::value<std::string>(&command_string)->required(), "START or STOP")
      ("period,p", po::value<int>(milli)->default_value(100),
       "Sampling period in milliseconds. Default: 100")
      ("output-file,o",  po::value<std::string>(ouputFilename)->default_value(""),
       "Full path of the file where to record sensor values on the robot "
       "(for instance: /home/nao/inmon.txt). Default: no output file");
  po::positional_options_description positionalOptions;
  positionalOptions.add("command", 1);

  po::variables_map vm; // Map containing all the options with their values
  try {
    po::store(po::command_line_parser(argc, argv).options(desc)
                .positional(positionalOptions).run(),
              vm);
    po::notify(vm);
    if (boost::to_upper_copy(command_string) == "START") {
      *command = START;
    } else if (boost::to_upper_copy(command_string) == "STOP") {
      *command = STOP;
    } else throw po::error("You may only specify START or STOP as command.");
  } catch(po::error &e) {
    std::cerr << e.what() << std::endl;
    std::cout << desc << std::endl;
    exit(1);
  }
}


int main(int argc, char* argv[]) {
  std::string parentBrokerIP, outputFile;
  int parentBrokerPort, periodInMilliseconds;
  command_t command;

  setlocale(LC_NUMERIC, "C"); // Need this to for SOAP serialization of floats to work

  parseOpt(&parentBrokerIP, &parentBrokerPort, &command, &periodInMilliseconds,
           &outputFile, argc, argv);

  boost::shared_ptr<AL::ALProxy> inertialMonitorProxy;
  try {
    inertialMonitorProxy = boost::shared_ptr<AL::ALProxy>
        (new AL::ALProxy("InertialMonitor", parentBrokerIP, parentBrokerPort));
  } catch (const AL::ALError& e) {
    std::cerr << "Could not create proxy: " << e.what() << std::endl;
    return 3;
  }

  if (command == STOP) {
    inertialMonitorProxy->callVoid("stopMonitor");
  } else { // (command == START}
    if (outputFile ==  "") {
      inertialMonitorProxy->callVoid("startMonitor", periodInMilliseconds);
    } else {
      inertialMonitorProxy->callVoid("startMonitorAndRecord", periodInMilliseconds,
                                     outputFile);
    }
  }

 return 0;
}
