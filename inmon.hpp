#ifndef INMON_HPP
#define INMON_HPP

#include <boost/shared_ptr.hpp>
#include <alcommon/almodule.h>
#include <string>
#include <exception>

namespace AL
{
// This is a forward declaration of AL:ALBroker which
// avoids including <alcommon/albroker.h> in this header
class ALBroker;
}

/**
 * This class inherits AL::ALModule to bind methods and be run as a plugin within NAOqi.
 */
class InertialMonitor : public AL::ALModule {
public:
  InertialMonitor(boost::shared_ptr<AL::ALBroker> pBroker, const std::string& pName);

  ~InertialMonitor();

  /**
   * Overrides ALModule::init() and ALModule::exit(). exit() is called before unloading.
   * init() is called right after the module has been loaded.
   */
  void init(), exit();

  /**
   * Start or restart the monitor with the given sampling period, without recording the
   * data locally.
   */
  void startMonitor(const int &milli);

  /**
   * Start or restart the monitor with the given sampling period, recording the data
   * locally in the given plain text output file. The data are appended to the file if it
   * already exists.
   */
  void startMonitorAndRecord(const int &milli, const std::string &file);

  /**
   * Stop the monitor, close the record file if any.
   */
  void stopMonitor();

private:
  struct Impl;
  boost::shared_ptr<Impl> impl;
};


class InertialMonitorError : public std::exception {
public:
  InertialMonitorError(const char* message);
  InertialMonitorError(const std::string message);
  virtual const char* what();

private:
  const char* msg;
};

#endif // INMON_HPP
